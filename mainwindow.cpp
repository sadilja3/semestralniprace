#include "mainwindow.h"

/*TODO:

Barva odpovedi X
Button repeat X
score X
Hscore X

*/
void MainWindow:: HiScore()
{
    if(Sco>HiSco)
    {
        HiSco=Sco;
    }
}
void MainWindow:: Score ()
{
    if(((Operace=="div")||(Operace=="mul"))&&(selected==false))
    {
        Sco+=2;

    }
    if(((Operace=="dif")||(Operace=="add"))&&(selected==false))
    {
        Sco++;
    }
}
void MainWindow:: ChAnsw ()
{
    QPushButton* AnsBut = qobject_cast<QPushButton*>(sender());
    QString Buttext = AnsBut->text();
    if(this->Correct==Buttext)
    {
       Butcol(AnsBut,'g');
       Score();
       rep->setVisible(true);
    }
    else
    {
       Butcol(AnsBut,'r');
       HiScore();
       Sco=0;
       selected=true;
       rep->setVisible(true);
    }
    if(selected==false){
       HiScore();
    }
    connect(rep, &QPushButton::clicked, this, &MainWindow::PriklWidg);

}

std::vector <int> MainWindow::Shuffle ()
{
    std::vector <int> shuff={0,1,2,3};
    std::random_shuffle(shuff.begin(), shuff.end());
    return shuff; // vracím pointer na pole Shuffled
}


std::vector <int> MainWindow::AnswDiv (int num) // naplní výsledkem a random hodnotami. První je vždy true value
{

    fflush(stdout);
    int rand = 0;
    std::vector answd={num};// inicializace 1. je true výsledek, další jsou prozatím 0
    int RVNum;
    float RV=1;
    for(int i=1;i<4;i++)// nactu zbyle pozice (3)
    {
        do{

        RV=QRandomGenerator::global()->bounded(-1 ,2);
        RVNum=RV*num+rand;
        rand++;
        }while((std::find(answd.begin(),answd.end(),RVNum)!=answd.end())||(RVNum==num));
        answd.push_back(RVNum);
    }
    return answd;// vracím pointer na pole divide
}

std::vector <int> MainWindow::Answ (int num) // naplní výsledkem a random hodnotami. První je vždy true value
{
    float rand = 0;
    fflush(stdout);
    std::vector answ={num};// inicializace 1. je true výsledek, další jsou prozatím 0
    int RVNum=0;
    for(int i=0;i<3;i++)// nactu zbyle pozice (3)
    {
        do{
            if(num>0)
            {
                fflush(stdout);
                rand=QRandomGenerator::global()->bounded(-1*num, 2*num+1);
            }
            else
            {
                fflush(stdout);
                rand=QRandomGenerator::global()->bounded(2*num, -1*num);
            }
            RVNum=num+rand;
        }while((std::find(answ.begin(),answ.end(),RVNum)!=answ.end())||(RVNum==num));// dělej dokud není naplněno rand hodnotami, ale ne stejnými jako jsou předchozí
        answ.push_back(RVNum);
    }
    return answ;// vracím pointer na pole
}
int MainWindow::rand (int Hi,int Lo)
{
    int RV = QRandomGenerator::global()->bounded(Lo, Hi);
    return RV;
}
QWidget* MainWindow::MenuWidg()// Main ve funkci
{
    //--------------------- LAYOUTS -------------------- Ma
    QGridLayout *layout = new QGridLayout(); // ALL
    QWidget *central = new QWidget();// defaultni menu
    central->setLayout(layout);
    QHBoxLayout *LBottom = new QHBoxLayout(); // Tlacitka odpovedi
    QHBoxLayout *LMiddle = new QHBoxLayout(); // "odpovedi" label
    QHBoxLayout *LTop = new QHBoxLayout(); // Priklad
    QHBoxLayout *LScore = new QHBoxLayout(); // Label Score: C
    QHBoxLayout *LBack= new QHBoxLayout(); // Button Zpět
    QGridLayout *LDiff= new QGridLayout();  // obtížnostní poznámky
    //-------------------Score patch -------------------- Ma/Pr
    Sco=0;
    //-------------------Buttons operace----------------- Ma
    QPushButton *plus= new QPushButton("+",this);
    plus->setFixedSize(100,100);
    Butcol(plus,'g');

    LBottom->addWidget(plus);
    QPushButton *minus= new QPushButton("-",this);
    minus->setFixedSize(100,100);
    Butcol(minus,'g');
    LBottom->addWidget(minus);

    QPushButton *div= new QPushButton("x",this);
    div->setFixedSize(100,100);
    Butcol(div,'r');
    LBottom->addWidget(div);
    QPushButton *mult= new QPushButton("/",this);
    mult->setFixedSize(100,100);
    Butcol(mult,'r');
    LBottom->addWidget(mult);
    //--------------- High Score ----------------------- Ma
    QLabel *LaHig = new QLabel(this);
    QString HiScore = QString("Nejvyšší skóre: %1").arg(HiSco);
    LaHig->setText(HiScore);
    LaHig->setAlignment(Qt::AlignRight);
    LaHig->setStyleSheet("QLabel { color : green; }");
    LScore->addWidget(LaHig);
    //---------------- Label Moznosti ------------------ Ma
    QLabel *LaOpt = new QLabel(this);
    LaOpt->setText("Možnosti:");
    LMiddle->addWidget(LaOpt);
    //---------------Labely Lehci/Tezsi----------------- Ma
    QLabel *LaLeh = new QLabel(this);
    LaLeh->setText("Lehčí příklady:");
    LaLeh->setAlignment(Qt::AlignLeft);
    LaLeh->setStyleSheet("QLabel { color : green; }");
    LDiff->addWidget(LaLeh,2,0);

    QLabel *LaTez = new QLabel(this);
    LaTez->setText("Těžší příklady:");
    LaTez->setAlignment(Qt::AlignLeft);
    LaTez->setStyleSheet("QLabel { color : red; }");
    LDiff->addWidget(LaTez,2,1);
    //------------ Button exit ------------------------- Ma
    QPushButton *exit= new QPushButton("odejít",this);
    LBack->addWidget(exit,0,Qt::AlignLeft);
    connect(exit, &QPushButton::clicked, this, &QCoreApplication::quit, Qt::QueuedConnection);
    //------------SHRNUTI LAYOUTU ---------------------- Ma
    layout->addLayout(LBack,0,0);
    layout->addLayout(LScore,1,0);
    layout->addLayout(LTop,2,0);
    layout->addLayout(LMiddle,3,0);
    layout->addLayout(LDiff,4,0);
    layout->addLayout(LBottom,5,0);

    connect(plus, &QPushButton::clicked, this, &MainWindow::ChanCentr);
    connect(minus, &QPushButton::clicked, this, &MainWindow::ChanCentr);
    connect(mult, &QPushButton::clicked, this, &MainWindow::ChanCentr);
    connect(div, &QPushButton::clicked, this, &MainWindow::ChanCentr);

    this->resize(400,300);
    this->setCentralWidget(central);
    return(central);
}
QWidget* MainWindow::PriklWidg()//PrCast ve funkci
{

    QGridLayout *layout2 = new QGridLayout(); // ALL pokud je pressed tlačítko
    QWidget *central2 = new QWidget();//pressed Widget
    //Widget

    central2->setLayout(layout2);
    QGridLayout *LayW = new QGridLayout();
    QHBoxLayout *LayBottom = new QHBoxLayout();
    QHBoxLayout *LayMiddle = new QHBoxLayout();
    QHBoxLayout *LayScore = new QHBoxLayout();
    QHBoxLayout *LayTop = new QHBoxLayout();
    QHBoxLayout *LayBack= new QHBoxLayout();
    QHBoxLayout *LayRep = new QHBoxLayout();
    //---------------- nastavení selected na false ----- Pr
    selected = false;
    //---------------- Label odpovedi ------------------ Pr
    QLabel *LaOdp = new QLabel(this);
    LaOdp->setText("Výsledky:");
    LayMiddle->addWidget(LaOdp);
    //------------- Label Příklad ---------------------- Pr
    QLabel *LaExc = new QLabel(this);
    QPushButton* signalbutton = qobject_cast<QPushButton*>(sender());
    QString buttonText = signalbutton->text();//Zachyti text buttonu, který byl zmáčknut a podle toho proběhne IF
    int V1=0;
    int V2=0;
    QString Vysl=0;
    std::vector <int> arr;
    if((buttonText.toStdString()=="/")||(Operace=="div")) // div
    {
        Operace = "div";
        V1=rand(75,1);
        V2=rand(5,1);
        int V3=V1*V2;
        QString text = QString("%1 / %2 = ?").arg(V3).arg(V1);
        Vysl=QString::number(V3/V1);
        arr = AnswDiv(Vysl.toInt());
        fflush(stdout);
        LaExc->setText(text);
        fflush(stdout);
    }
    if((buttonText.toStdString()=="x")||(Operace=="mul"))
    {
       Operace = "mul";
       do{
       V1=rand(12,0);
       V2=rand(12,0);
       }while ((V1*V2)<10);

       QString text = QString("%1 x %2 = ?").arg(V1).arg(V2);
       Vysl=QString::number(V1*V2);
       arr = this->Answ(Vysl.toInt());
       LaExc->setText(text);
    }
    if((buttonText.toStdString()=="+")||(Operace=="add"))
    {
       Operace = "add";
       V1=rand(100,0);
       V2=rand(100,0);
       QString text = QString("%1 + %2 = ?").arg(V1).arg(V2);
       Vysl=QString::number(V1+V2);
       arr = this->Answ(Vysl.toInt());
       LaExc->setText(text);
    }
    if((buttonText.toStdString()=="-")||(Operace=="dif"))
    {
       Operace = "dif";
       fflush(stdout);
       V1=rand(100,0);
       V2=rand(100,0);
       QString text = QString("%1 - %2 = ?").arg(V1).arg(V2);
       Vysl=QString::number(V1-V2);
       arr = this->Answ(Vysl.toInt());
       LaExc->setText(text);
    }
    LaExc->setStyleSheet("font: 24pt;");
    LaExc->setAlignment(Qt::AlignCenter);
    LayTop->addWidget(LaExc);

    //------------------SHUFFLE + Odpovědi tLačítka--------------------------- Pr
    std::vector <int> Shuff = Shuffle();

    QPushButton *A= new QPushButton(QString::number(arr[Shuff[0]]),this);
    A->setFixedSize(100,100);
    LayBottom->addWidget(A);
    QPushButton *B= new QPushButton(QString::number(arr[Shuff[1]]),this);
    B->setFixedSize(100,100);
    LayBottom->addWidget(B);
    QPushButton *C= new QPushButton(QString::number(arr[Shuff[2]]),this);
    C->setFixedSize(100,100);
    LayBottom->addWidget(C);
    QPushButton *D= new QPushButton(QString::number(arr[Shuff[3]]),this);
    D->setFixedSize(100,100);
    LayBottom->addWidget(D);

    rep= new QPushButton("Další?",this);
    rep->setFixedSize(100,30);
    rep->setVisible(false);
    LayRep->addWidget(rep);

    //-----------------Connect Sparávná odpověď ------- Pr

    Correct = QString::number(arr[0]);
    //std::cout<<"Correct:"<<arr[0]<<std::endl;
    connect(A, &QPushButton::clicked, this, &MainWindow::ChAnsw);
    connect(B, &QPushButton::clicked, this, &MainWindow::ChAnsw);
    connect(C, &QPushButton::clicked, this, &MainWindow::ChAnsw);
    connect(D, &QPushButton::clicked, this, &MainWindow::ChAnsw);

    //------------- Score: txt ------------------------- Pr

    QString Score = QString("Score: %1").arg(Sco);
    QLabel *LaSco = new QLabel(this);
    LaSco->setText(Score);
    LaSco->setAlignment(Qt::AlignRight);
    LaSco->setStyleSheet("QLabel { color : green; }");
    LayScore->addWidget(LaSco);

    //------------ Button zpet ------------------------- Pr

    QPushButton *back= new QPushButton("zpět",this);
    connect(back, &QPushButton::clicked,this , &MainWindow::MenuWidg);

    //------------Kompletování příkladu ----------------- Pr

    LayBack->addWidget(back,0,Qt::AlignLeft);
    layout2->addLayout(LayBack,0,0);
    layout2->addLayout(LayScore,1,0);
    layout2->addLayout(LayTop,2,0);
    layout2->addLayout(LayRep,3,0);
    layout2->addLayout(LayMiddle,4,0);
    layout2->addLayout(LayW,5,0);
    layout2->addLayout(LayBottom,6,0);
    this->setCentralWidget(central2); // při button repeat resetnou central2NEW za centra2OLD

    return central2;
}


void MainWindow::ChanCentr()// Propojení Tlačítka + a příkladové části
{
    QWidget *central2 = MainWindow::PriklWidg();
    this->setCentralWidget(central2);
}


void MainWindow::Butcol (QPushButton *b, char c)
{
    if(c=='g')
    {
        QPalette pal = b->palette();
        pal.setColor(QPalette::Button, QColor(Qt::green));
        b->setAutoFillBackground(true);
        b->setPalette(pal);
        b->update();
    }
    if(c=='r')
    {
        QPalette pal = b->palette();
        pal.setColor(QPalette::Button, QColor(Qt::red));
        b->setAutoFillBackground(true);
        b->setPalette(pal);
        b->update();
    }
}


MainWindow::MainWindow(QWidget *parent)//main
    : QMainWindow(parent)
{
    QWidget *central = new QWidget();
    central=MenuWidg();
    this->resize(400,300);
    this->setCentralWidget(central);
}

MainWindow::~MainWindow()
{

}

