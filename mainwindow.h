#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//includovaní potřebných knihoven

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QAbstractButton>
#include <QRandomGenerator>
#include <iostream>
#include <algorithm>
#include <random>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString Correct;//správný výsledek v typu Qstring pro možnost porovnání
    bool selected;//zapamatování, zda-li si uživatel již vybral tlačítko s odpovědí
    int HiSco=0;//Max score
    int Sco=0;//Score aktuální
    QString Operace;//zapamatování, jakou oparci si uživatel zvolil
    MainWindow(QWidget *parent = nullptr);
    void Butcol(QPushButton *b,char c);//zbravení tlačítka(jakého,barva)
    void ChanCentr();//změna z Menu do Příkladu
    QWidget* PriklWidg();//Příkladový Widget
    QWidget* MenuWidg();//Menu Widget
    std::vector <int> Answ(int num);//vygenerované výsledky příkladu (na 1. pozici je správná hodnota)
    std::vector <int> AnswDiv(int num);//vygenerované výsledky příkladu (na 1. pozici je správná hodnota) pro dělení (aby byly výsledky celá čísla)
    int rand(int Hi,int Lo);//náhodná hodnota (max,min)
    std::vector <int> Shuffle();//Vektor náhodně naplněný hodnotami 0,1,2,3
    void ChAnsw();//Kontrola, zda-li je zvolená odpověď true
    void Score();// Funkce pro určení získaného score (+,-) = 1b (x,/) = 2b
    void HiScore();//funkce pro nastavení High Score pokud Sco je větší jak HiSco
    QPushButton *rep;// repeat button




    ~MainWindow();


private:

};
#endif // MAINWINDOW_H
